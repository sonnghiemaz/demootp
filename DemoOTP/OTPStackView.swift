//
//  OTPStackView.swift
//  DemoOTP
//
//  Created by Apple on 19/07/2022.
//

import Foundation
import UIKit

protocol OTPDelegate: AnyObject {
    //always triggers when the OTP field is valid
    func didChangeValidity(isValid: Bool)
}

class OTPStackView: UIStackView {
    
    //Customize OTPField
    let numOfFields = 4
    var textFieldsCollection: [OTPTextField] = []
    weak var delegate: OTPDelegate?
    let emptyFieldBorderColor = UIColor.gray
    let textBgColor = UIColor(white: 1, alpha: 0.5)
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupStackView()
        addOTPFields()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStackView()
        addOTPFields()
    }
    
    //Setting StackView
    private final func setupStackView() {
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = true
        self.spacing = 10
        self.distribution = .fillEqually
        self.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    //Adding each OTPField to stackview
    private final func addOTPFields() {
        for index in 0..<numOfFields {
            let field = OTPTextField()
            setupTextField(field)
            textFieldsCollection.append(field)
            //Adding a marker to previous field
            index != 0 ? (field.prevTextField = textFieldsCollection[index-1]) : (field.prevTextField = nil)
            //Adding a marker to next field for the field at index - 1
            index != 0 ? (textFieldsCollection[index-1].nextTextField = field) : ()
        }
        textFieldsCollection[0].becomeFirstResponder()
    }
    
    //Setting OTPTextFields
    private final func setupTextField(_ textField: OTPTextField) {
        textField.delegate = self
        self.addArrangedSubview(textField)
        textField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        textField.widthAnchor.constraint(equalToConstant: 50).isActive = true
        textField.backgroundColor = textBgColor
        textField.font = UIFont(name: "Arial", size: 30)
        textField.textColor = .black
        textField.layer.cornerRadius = 10
        textField.layer.borderWidth = 2
        textField.layer.borderColor = emptyFieldBorderColor.cgColor
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
        textField.autocorrectionType = .yes
        textField.textContentType = .oneTimeCode
        
    }
    
    //check if all the field are filled
    private final func checkForValidity() {
        for field in textFieldsCollection {
            if (field.text?.trimmingCharacters(in: CharacterSet.whitespaces) == "") {
                delegate?.didChangeValidity(isValid: false)
                return
            }
        }
        delegate?.didChangeValidity(isValid: true)
    }
    
    //back to where it all started
    final func resetAllField() {
        for tf in textFieldsCollection {
            tf.text = ""
            tf.layer.borderColor = emptyFieldBorderColor.cgColor
        }
        delegate?.didChangeValidity(isValid: false)
    }
}

//MARK: - Textfield Handling
extension OTPStackView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkForValidity()
        textField.layer.borderColor = textField.text == "" ? emptyFieldBorderColor.cgColor : UIColor.red.cgColor
    }
    
    //switch between textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? OTPTextField else {return true}
        if string.count > 1 {
            textField.resignFirstResponder()
            return false
        } else {
            if (range.length == 0 && string == "") {
                return false
            } else if (range.length == 0) {
                if textField.nextTextField == nil {
                    textField.text? = string
                    textField.resignFirstResponder()
                } else {
                    textField.text? = string
                    textField.nextTextField?.becomeFirstResponder()
                }
                return false
            }
            return true
        }
    }
}
