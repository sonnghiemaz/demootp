//
//  OTPTextField.swift
//  DemoOTP
//
//  Created by Apple on 19/07/2022.
//

import Foundation
import UIKit

class OTPTextField: UITextField {
    weak var prevTextField: OTPTextField?
    weak var nextTextField: OTPTextField?
    
    override func deleteBackward() {
        text = ""
        prevTextField?.becomeFirstResponder()
    }
}
