//
//  ViewController.swift
//  DemoOTP
//
//  Created by Apple on 19/07/2022.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var verifyBtn: UIButton!
    
    let otpStackView = OTPStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verifyBtn.isHidden = true
        otpView.addSubview(otpStackView)
        otpStackView.delegate = self
    }

    @IBAction func pressedVerify(_ sender: UIButton) {
        otpStackView.resetAllField()
    }
}

extension ViewController: OTPDelegate {
    func didChangeValidity(isValid: Bool) {
        verifyBtn.isHidden = !isValid
    }
}
